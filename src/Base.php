<?php

namespace Brainex\VerifiedAfrica;

use Brainex\VerifiedAfrica\Exception\VerifyException;

abstract class Base
{
    private static $user_id;

    protected $base_uri = 'https://api.verified.africa/sfx-verify/v3/id-service/';

    protected $timeout = 50;

    /**
     * Send request
     *
     * @param string $apikey
     * @param array $payload
     * @return mixed
     */
    protected function request(string $apikey, array $payload)
    {
        $ch = curl_init($this->base_uri);
        $headers = array(
            'apikey: ' . $apikey,
            'userid: '. static::$user_id,
            'content-type: application/json'
        );

        $postfields = json_encode($payload);

        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $postfields,
            CURLOPT_TIMEOUT => $this->timeout,
            CURLOPT_CONNECTTIMEOUT => $this->timeout,
            CURLOPT_HTTPHEADER => $headers
        ]);

        $data = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        $http_code = $info['http_code'];

        if(!($http_code >= 200 && $http_code <= 299)) {
            throw new VerifyException($data);
        }

        return json_decode($data);
    }

    /**
     * Set user Id
     *
     * @param string $user_id
     * @return void
     */
    public static function setUserId(string $user_id)
    {
        static::$user_id = $user_id;
    }
}