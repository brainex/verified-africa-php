<?php

namespace Brainex\VerifiedAfrica;

class Bvn extends Base
{
    /**
     * BVN full service verification
     *
     * @param string $apikey
     * @param string $search_parameter
     * @param string|null $reference
     * @return object
     */
    public function fullService(string $apikey, string $search_parameter, ?string $reference = null)
    {
        return $this->request($apikey, [
            'searchParameter' => $search_parameter,
            'transactionReference' => $reference,
            'verificationType' => 'BVN-FULL-DETAILS'
        ]);
    }

    /**
     * Boolean match
     *
     * @param string $apikey
     * @param array $payload
     * @see https://verifiedng.readme.io/v3.0/reference#bvn-boolean-match
     * @return object
     */
    public function booleanMatch(string $apikey, array $payload)
    {
        $payload['verificationType'] = 'BVN-VERIFICATION';
        return $this->request($apikey, $payload);
    }
}