<?php

namespace Brainex\VerifiedAfrica;

class Nimc extends Base
{
    public function verificationByNin(string $apikey, string $search_parameter, ?string $reference = null)
    {
        return $this->request($apikey, [
            'searchParameter' => $search_parameter,
            'transactionReference' => $reference,
            'verificationType' => 'NIN-SEARCH'
        ]);
    }

    public function verificationByPhoneNumber(string $apikey, string $search_parameter, ?string $reference = null)
    {
        return $this->request($apikey, [
            'searchParameter' => $search_parameter,
            'transactionReference' => $reference,
            'verificationType' => 'NIN-PHONE-SEARCH'
        ]);
    }
}